#!/bin/python

from bottle import request,response
import bottle
import json
import slack
import logging
import logging.handlers
import time

# Logging configuration
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address = '/dev/log')
formatter = logging.Formatter('dragonbot.%(module)s.%(funcName)s: %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

# Initialize Bottle application
app = application = bottle.Bottle()

# Return placeholder for GET requests to /
@app.get('/')
def show_index():
    return 'DragonBot says hello!'

# Return placeholder for GET requests to /DragonBot
@app.get('/dragonbot')
@app.get('/dragonbot/')
def show_hello():
    return 'DragonBot says hello!'

# Endpoint for Slack OAuth flow
@app.get('/dragonbot/oauth')
@app.get('/dragonbot/oauth/')
def oauth():
    log.info('Entering app.oauth()')
    try:
        code = request.query.code
        log.info('Received oauth code: %s' % code)
    except:
        log.warn('Unable to parse oauth code')
        response.status = 400
        return '400 Bad Request'
    return slack.oauth(code)

# Endpoint for Slack event subscriptions
@app.post('/dragonbot/event')
@app.post('/dragonbot/event/')
def event():
    log.info('Entering app.event()')
    log.debug('Attempting auth header translation')
    try:
        log.debug('Extract request.headers[\'X-Slack-Request-Timestamp\']')
        ts = str(request.headers['X-Slack-Request-Timestamp'])
        log.debug('ts = %s' % ts)
        if abs(time.time() - float(ts)) > 150:
            log.warn('Request time differs by %s seconds - rejecting request' % str(abs(time.time() - float(ts))))
            response.status = 403
            return '403 Forbidden'
        log.debug('Extract requests.headers[\'X-Slack-Signature\']')
        sig = str(request.headers['X-Slack-Signature'])
        log.debug('sig = %s' % sig)
        log.debug('Extract request.body')
        body = request.body.read()
        log.debug('Header extraction complete')
    except:
        log.error('Auth header extraction failed')
        response.status = 403
        return '403 Forbidden'
    log.debug('Exec slack.auth_request()')
    if not slack.auth_request(ts,sig,body):
        log.warn('Authorization failed - returning 403 Forbidden')
        response.status = 403
        return '403 Forbidden'
    data = request.json
    response.headers['Content-Type'] = 'application/json'
    return slack.event(data)

# Endpoint for Slack interactive message callbacks
@app.post('/dragonbot/callback')
@app.post('/dragonbot/callback/')
def callback():
    log.info('Entering app.callback()')
    log.debug('Attempting auth header translation')
    try:
        log.debug('Extract request.headers[\'X-Slack-Request-Timestamp\']')
        ts = str(request.headers['X-Slack-Request-Timestamp'])
        log.debug('ts = %s' % ts)
        if abs(time.time() - float(ts)) > 150:
            log.warn('Request time differs by %s seconds - rejecting request' % str(abs(time.time() - float(ts))))
            response.status = 403
            return '403 Forbidden'
        log.debug('Extract requests.headers[\'X-Slack-Signature\']')
        sig = str(request.headers['X-Slack-Signature'])
        log.debug('sig = %s' % sig)
        log.debug('Extract request.body')
        body = request.body.read()
        log.debug('Header extraction complete')
    except:
        log.error('Auth header extraction failed')
        response.status = 403
        return '403 Forbidden'
    log.debug('Exec slack.auth_request()')
    if not slack.auth_request(ts,sig,body):
        log.warn('Authorization failed - returning 403 Forbidden')
        response.status = 403
        return '403 Forbidden'
    d = request.forms.get('payload')
    response.headers['Content-Type'] = 'application/json'
    return slack.callback(d)

# Endpoint for Slack slash commands
# Note: There are currently no slash commands in use
@app.post('/dragonbot/command')
@app.post('/dragonbot/command/')
def command():
    log.info('Entering app.command()')
    log.debug('Attempting auth header translation')
    try:
        log.debug('Extract request.headers[\'X-Slack-Request-Timestamp\']')
        ts = str(request.headers['X-Slack-Request-Timestamp'])
        log.debug('ts = %s' % ts)
        if abs(time.time() - float(ts)) > 150:
            log.warn('Request time differs by %s seconds - rejecting request' % str(abs(time.time() - float(ts))))
            response.status = 403
            return '403 Forbidden'
        log.debug('Extract requests.headers[\'X-Slack-Signature\']')
        sig = str(request.headers['X-Slack-Signature'])
        log.debug('sig = %s' % sig)
        log.debug('Extract request.body')
        body = request.body.read()
        log.debug('Header extraction complete')
    except:
        log.error('Auth header extraction failed')
        response.status = 403
        return '403 Forbidden'
    log.debug('Exec slack.auth_request()')
    if not slack.auth_request(ts,sig,body):
        log.warn('Authorization failed - returning 403 Forbidden')
        response.status = 403
        return '403 Forbidden'
    try:
        token = request.forms.get('token')
        cmd = request.forms.get('command')
        data = request.forms.get('text')
    except:
        raise ValueError
    result = slack.command(cmd, data)
    response.headers['Content-Type'] = 'application/json'
    return result
