#!/usr/local/bin/python3.7

import json
import requests
import json
import hmac
import hashlib
import logging
import logging.handlers
import pgdb
import codecs

# Logging configuration
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address = '/dev/log')
formatter = logging.Formatter('dragonbot.%(module)s.%(funcName)s: %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

url = 'https://slack.com/api/'

# Authenticate all received requests with signed secrets, except OAuth requests
def verify_request(ts, rcv_sig, body):
    log.info('Authenticating request')
    log.debug(f'Received signature "{rcv_sig}" with timestamp "{ts}"')
    key = pgdb.get_key_value('dragonbot_sign_sec') 
    if not key:
        log.error('Signing key missing from database, check "dragonbot_sign_sec"')
        return False
    try:
        log.info('Computing HMAC')
        cmp_sig = 'v0='+hmac.new(codecs.encode(key), b'v0:'+codecs.encode(ts)+b':'+body, hashlib.sha256).hexdigest()
    except ValueError as e:
        log.warn(f'Unable to encode signature parameter as UTF-8: {repr(e)}')
        cmp_sig = '0'
    except BaseException as e:
        log.warn(f'HMAC computation failed: {repr(e)}')
        cmp_sig = '0'
    log.debug(f'Calculated signature: {cmp_sig}')
    result = hmac.compare_digest(cmp_sig, rcv_sig)
    if result:
        log.info('Request successfully authenticated')
    else:
        log.warn('Request not authenticated - invalid message signature')
    return result


# Parse message to DragonBot to split into title and body
def parse_title(text):
    log.info('Parsing issue for title')
    log.debug(f'Issue text: {text}')
    sep = ':'
    n = text.find(sep)
    log.info(f'Separator found at position: {n}')
    # Colon found as first character, remove it
    if n == 0:
        log.debug('First character is a separator, stripping first character')
        text = text[1:]
    # Colon found within the first 80 characters, split title and body at colon
    if 0 < n <= 80:
        log.debug(f'Separator found in first 80 characters splitting at {n}')
        return [text[0:n],text[n+1:]]
    # Colon found after the first 80 chars, split on the earlier of either:
    # first space after 60th character, or 75 characters. Add ellipsis.
    if n > 80:
        log.debug('Separator found, but after the 8th character')
        cutoff = min(max(text.find(' ', 60), 60), 75)
        log.debug(f'Splitting at position: {cutoff}')
        return [text[0:cutoff]+'...', text]
    if len(text) > 80:
        log.debug('No separator found, text longer than 80 characters')
        cutoff = min(max(text.find(' ', 60), 60), 75)
        log.debug(f'Splitting at position: {cutoff}')
        return [text[0:cutoff]+'...', text]
    log.debug('No separator found, text not longer than 80 characters. Returning all text as title.')
    return [text, '']


# Retrieve the channel_id for a give channel name
def get_channel_id(channel, team_id):
    log.info(f'Retrieving channel_id for channel: {channel}, team_id: {team_id}')
    token = pgdb.get_key_value('slack_bot_token', team_id)
    if not token:
        log.error('Token failure')
        return None
    log.info('Requesting channels list')
    r = requests.get(url+'channels.list', headers={'Authorization':f'Bearer {token}'})
    if r.status_code != 200:
        log.warn(f'Unable to locate channel list with HTTP status {r.status_code}, response: {r.text}')
        return None
    log.info('Channel list retrieved, parsing JSON')
    try:
        resp = r.json()
    except:
        log.error(f'Unable to parse channel response: {r.text}')
    if 'channels' not in resp:
        log.error(f'Channel list not returned in response: {r.text}')
        return None
    for i in resp['channels']:
        log.debug(f"Channel {i['name']}: ID {i['id']}")
        if i['name'] == channel:
            log.info(f"Channel '{channel}' located, ID: {i['id']}")
            return i['id']
    log.warn(f"Channel '{channel}' not found! Using general instead.")
    for i in resp['channels']:
        log.debug(f"Channel {i['name']}: ID {i['id']}")
        if i['name'] == 'general':
            log.info(f"General channel ID: {i['id']}")
            return i['id']
    log.warn('Unable to locate general channel')
    return None


# Receive OAuth code from initial user authorization and exchange for workspace tokens when user adds bot to workspace
def oauth(code):
    client_id = pgdb.get_key_value('clntid')
    client_sec = pgdb.get_key_value('clntsec')
    # Generate code exchange body
    payload = {}
    payload['client_id'] = client_id
    payload['client_secret'] = client_sec
    payload['code'] = code
    payload['redirect_uri'] = 'https://12oz.glass/dragonbot/oauth'
    # Send code exchange request as x-www-form-urlencoded body per oauth2 spec
    r = requests.post(url+'oauth.access', data=payload)
    if r.status_code != 200:
        log.warn(f'Unsuccessful token exchange with HTTP status {r.status_code}: {r.text}')
        return 'Unable to authenticate to Slack. This was not your fault.'
    try:
        resp = r.json()
    except: 
        log.warn(f'Unable to parse token exchange JSON response: {r.text}')
    if not resp['ok']:
        log.warn(f'Unsuccessful token exchange, response declared not ok: {r.text}')
        return 'Unable to authenticate to Slack. This was not your fault.'
    # Log exchanged credentials to database
    try:
        team_id = resp['team_id']
        user_token = resp['access_token']
        bot_token = resp['bot']['bot_access_token']
        log.info(f'Successfully exchanged OAuth code for token for workspace {team_id}')
    except KeyError as e:
        log.warn(f'Received success code from token exchange, but missing key value in exchange response: {repr(e)}')
    except BaseException as e:
        log.warn(f'Received success code from token exchange, but encountered unknown error retrieving values: {repr(e)}')
    log.info('Writing tokens to database')
    if pgdb.upsert_key_value('slack_bot_token', bot_token, team_id):
        log.info('Successfully wrote slack_bot_token to database')
    else:
        log.warn('Failed to write slack_bot_token to database, see previous errors')
    if pgdb.upsert_key_value('slack_user_token', user_token, team_id):
        log.info('Successfully wrote slack_user_token to database')
    else:
        log.warn('Failed to write slack_user_token to database, see previous errors')
    # Return simple response body to display to authorizing user
    return 'App authorization successful, thanks for using DragonBot!'


# Process incoming event subscription notifications, interested in received DMs
def event(data):
    log.info(f"DragonBot received event of type {data['type']}")
    if not data:
        log.info('Event received with no data, ignoring')
        return ''
    # If we received a periodic validation request from Slack, respond to the challenge
    if 'type' in data.keys():
        if data['type'] == 'url_verification':
            log.info('Received URL verification')
            log.info(f"Returning challenge: {data['challenge']}")
            return json.dumps({'challenge':data['challenge']})
    # Handle actual subscribed events received that we are ignoring: message updates or events for messages the bot sends
    if 'event' in data.keys():
        if 'bot_id' in data['event'].keys():
            log.info('Ignoring event generated by bot user')
            return ''
        if 'subtype' in data['event'].keys():
            if data['event']['subtype'] == 'message_deleted' or data['event']['subtype'] == 'message_changed':
                log.info(f"Ignoring event with subtype: {data['event']['subtype']}")
                return ''
    # If we receive a new DM message to the bot user, confirm issue creation 
    if 'event' in data.keys():
        if data['event']['type'] == 'message' and data['event']['channel_type'] == 'im':
            log.info(f"Received event, type: {data['event']['type']}")
            # Reply to message with preview and confirmation buttons
            if confirm_issue(data):
                log.info('Issue confirmation created')
            else:
                log.warn('Failed to create issue, see previous errors')
    # Not safe to remove empty return line - must provide response body even if empty.
    return ''

# Process callback from interactive message buttons, always returning empty string deliberately
def callback(data):
    try:
        d = json.loads(data)
    except:
        log.warn(f'Unable to parse callback JSON: {data}')
        return ''
    if not 'actions' in d.keys():
        log.warn('Callback event received with no actions')
        return ''
    if not 'name' in d['actions'][0].keys():
        log.warn('Callback event received with no actions.name')
        return ''
    log.info(f"Callback event: {d['actions'][0]['name']}")
    # User clicked "Resolve issue" button
    if d['actions'][0]['name'] == 'resolved':
        return resolve_issue(d)
    # User clicked "Reopen issue" button
    elif d['actions'][0]['name'] == 'reopen':
        return reopen_issue(d)
    # User clicked button to create previewed issue
    elif d['actions'][0]['name'] == 'create':
        return create_issue(d)
    # User clicked button to cancel previewed issue
    elif d['actions'][0]['name'] == 'cancel':
        return cancel_issue(d)
    else:
        log.warn(f"Callback with unrecognized actions.name: {d['actions'][0]['name']}")
        log.debug(f'Callback data: {d}')
        return ''


# Preview and confirm submitted issue with user before creation
def confirm_issue(data):
    log.info('Confirming issue')
    try:
        text = data['event']['text']
        channel = data['event']['channel']
        log.debug(f'Confirming issue in channel {channel}: {text}')
    except:
        log.warn('Unable to load event data to confirm issue')
        return False
    title, body = parse_title(text)
    team_id = data['team_id']
    log.debug(f'Team ID: {team_id}')
    try:
        token = pgdb.get_key_value('slack_bot_token', team_id)
    except BaseException as e:
        log.error(f'Failed to retrieve token: {repr(e)}')
        return False
    attach = [{
        'callback_id':'issue_create',
        'title':title,
        'text':body,
        'attachment_type':'default',
        'color':'#3AA3E3', # light blue
        'actions':[{
            'name':'create',
            'value':'create',
            'text':'Create issue',
            'type':'button',
            'style':'primary'
        },{
            'name':'cancel',
            'value':'cancel',
            'text':'Cancel',
            'type':'button',
            'style':'danger'
        }]
    }]
    payload = {}
    payload['channel'] = channel
    payload['text'] = 'Would you like me to create this issue?'
    payload['attachments'] = attach
    log.info('Posting issue confirmation')
    log.debug(f'POSTing: {payload}')
    r = requests.post(url+'chat.postMessage', headers={'Authorization': f'Bearer {token}'}, json=payload)
    if r.status_code == 200:
        log.info('Issue confirmation message sent successfully')
    else:
        log.warn(f'Issue confirmation message failed with HTTP status {r.response_code}, response body: {r.text}')
        return False
    return True


# Create issue in channel after preview is confirmed
def create_issue(data):
    title,body = '',''
    try:
        if 'title' in data['original_message']['attachments'][0]:
            title = data['original_message']['attachments'][0]['title']
        if 'text' in data['original_message']['attachments'][0]:
            body = data['original_message']['attachments'][0]['text']
    except:
        log.error('Unable to parse title and body')
    payload = {}
    team_id = data['team']['id']
    log.info(f'Team ID: {team_id}')
    token = pgdb.get_key_value('slack_bot_token', team_id)
    if not token:
        log.error('Failed to retrieve token')
        return False
    auth = f'Bearer {token}'
    attach = [{
        'callback_id':'issue_create',
        'title':title,
        'text':body,
        'attachment_type':'default',
        'color':'#D13C3C', # red
        'actions':[{
            'name':'resolved',
            'value':'resolved',
            'text':'Mark as Resolved',
            'type':'button',
            'style':'primary',
            'confirm':{
                'text':'Confirm issue resolution',
                'dismiss_text':'Go Back',
                'ok_text':'Confirmed',
                'title':'Confirm'
            }
        }]
    }]
    channel = get_channel_id('issues', team_id)
    if not channel:
        log.warn('Failed to retrieve a valid channel ID, unable to create issue')
        # Respond to user with failure message
        return 'Failed to create issue'
    log.info('Posting issue')
    r = requests.post(url+'chat.postMessage', headers={'Authorization':auth},
    json={'channel': channel, 'attachments':attach})
    log.info(f'Issue create response status code: {r.status_code}')
    resp = r.json()
    payload = {}
    if r.status_code == 200 and resp['ok'] and 'message' in resp:
        ts = resp['message']['ts']
        payload['channel'] = resp['channel']
        payload['thread_ts'] = ts
        payload['text'] = 'Please use this thread for discussion of this issue.'
        log.info('Posting thread message')
        r = requests.post(url+'chat.postMessage', headers={'Authorization':auth}, json=payload)
        if r.status_code == 200:
            log.info(f'Thread message created successfully')
        else: 
            log.warn(f'Thread message failed with HTTP status {r.status_code}: {r.text}')
        try:
            log.info(f'Logging issue ts {ts}')
            pgdb.log_issue(ts, title, body)
        except:
            log.warn(f'Issue ts {ts} not logged!')
        else:
            log.info(f'Logged issue with ts {ts} to database')
    else:
        log.warn(f"Issue creation failed with HTTP status {r.status_code}, ok: {resp['ok']}, 'message' {'not' if 'message' not in resp else ''} present")
    attach = [{
        'callback_id':'issue_created',
        'title':title,
        'text':body,
        'attachment_type':'default',
        'color':'#1DAD76',
    }]
    log.info('Updating confirmation message')
    return json.dumps({'text':'Copy that, issue has been created!', 'attachments':attach})


# Cancel a previewed issue awaiting confirmation
def cancel_issue(data):
    team_id = data['team']['id']
    log.info(f'Team ID: {team_id}')
    token = pgdb.get_key_value('slack_bot_token', team_id)
    if not token:
        log.error('Token failure')
        return False
    auth = f'Bearer {token}'
    payload = {}
    payload['channel'] = data['channel']['id']
    payload['ts'] = data['original_message']['ts']
    log.info('Posting delete message')
    r = requests.post(url+'chat.delete', headers={'Authorization':auth}, json=payload)
    if r.status_code == 200:
        log.info(f'Delete message response status: {r.status_code}')
    else:
        log.warn(f'Unable to delete cancelled issue with HTTP status {r.status_code}: {r.text}')
    return ''


# Resolve an active issue
def resolve_issue(data):
    title,body = '',''
    if 'title' in data['original_message']['attachments'][0].keys():
        title = data['original_message']['attachments'][0]['title']
    if 'text' in data['original_message']['attachments'][0].keys():
        body = data['original_message']['attachments'][0]['text']
    try:
        blame = f"by <@{data['user']['id']}>"
    except KeyError:
        blame = ''
        log.warn('User ID not found in payload, resolving issue with noone to blame')
    except BaseException as e:
        blame = ''
        log.warn(f'Unable to set blame for unknown reason, resolving issue with noone to blame. Error: {repr(e)}')
    attach = [{
        'callback_id':'issue_resolve',
        'title':title,
        'text':body,
        'footer':f'Issue has been resolved {blame}',
        'footer_icon':'https://12oz.glass/static/checkbox.png',
        'attachment_type':'default',
        'color':'#1DAD76', # green
    }]
    try:
        thread_ts = data['original_message']['thread_ts']
    except:
        log.error('Resolve received with no thread_ts')
        log.debug(f'Callback data: {data}')
        log.warn('Resolving issue but not posting a re-open message')
        return json.dumps({'attachments':attach})
    team_id = data['team']['id']
    log.info(f'Team ID: {team_id}')
    token = pgdb.get_key_value('slack_bot_token', team_id)
    if not token:
        log.warn('Resolving issue but not posting a re-open message')
        token = None
    if token:
        auth = f'Bearer {token}'
    else:
        auth = None
    # Update original issue message to be resolved
    payload = {}
    payload['channel'] = data['channel']['id']
    payload['thread_ts'] = thread_ts
    payload['attachments'] = [{
        'callback_id':'issue_reopen',
        'text':f'Issue has been resolved {blame}',
        'attachment_type':'default',
        'color':'#1DAD76', # green
        'actions':[{
            'name':'reopen',
            'value':'reopen',
            'text':'Re-open',
            'type':'button',
            'style':'danger',
            'confirm':{
                'text':'Are you sure you want to re-open this issue?',
                'dismiss_text':'Go Back',
                'ok_text':'Confirmed',
                'title':'Confirm'
            }
        }]
    }]
    log.info('Posting re-open message')
    if auth:
        r = requests.post(url+'chat.postMessage',headers={'Authorization':auth},json=payload)
    log.info(f'Re-open message request status r.status_code')
    log.debug(f'Re-open message request message is: {r.json()}')
    log.info('Returning resolved message')
    # Post resolved issue message to thread
    return json.dumps({'attachments':attach})

# Reopen a resolved issue
def reopen_issue(data):
    try:
        thread_ts = data['original_message']['thread_ts']
    except:
        log.error('Reopen received with no thread_ts')
        log.debug(f'Callback data: {data}')
        return ''
    log.info(f'Reopen issue with thread_ts {thread_ts}')
    try:
        blame = f"by <@{data['user']['id']}>"
    except KeyError:
        blame = ''
        log.warn('User ID not found in payload, reopening issue with noone to blame')
    except BaseException as e:
        blame = ''
        log.warn(f'Unable to set blame for unknown reason, resolving issue with noone to blame. Error: {repr(e)}')
    title, body = pgdb.retrieve_issue(thread_ts)
    team_id = data['team']['id']
    log.info(f'Team ID: {team_id}')
    token = pgdb.get_key_value('slack_bot_token', team_id)
    if not token:
        log.error('Failed to retrieve token, cannot reopen issue')
        return False
    auth = f'Bearer {token}'
    # Update original message to be an open issue
    attach = [{
        'callback_id':'issue_resolve',
        'title':title,
        'text':body,
        'attachment_type':'default',
        'color':'#D13C3C',
        'actions':[{
            'name':'resolved',
            'value':'resolved',
            'text':'Mark as Resolved',
            'type':'button',
            'style':'primary',
            'confirm':{
                'text':'Confirm issue resolution',
                'dismiss_text':'Go Back',
                'ok_text':'Confirmed',
                'title':'Confirm'
            }
        }]
    }]
    payload = {}
    payload['ts'] = thread_ts
    payload['channel'] = get_channel_id('issues', team_id)
    payload['attachments'] = attach
    log.info('Updating original message to be re-opened')
    r = requests.post(url+'chat.update', headers={'Authorization':auth}, json=payload)
    if r.status_code == 200:
        log.info(f'Reopen issue response status: {r.status_code}')
    else:
        log.warn(f'Unable to reopen issue with HTTP status {r.status_code}: {r.text}')
    body = ''
    if 'text' in data['original_message']['attachments'][0].keys():
        body = data['original_message']['attachments'][0]['text']
    attach = [{
        'callback_id':'issue_resolve',
        'text':body,
        'footer':f'Issue has been re-opened {blame}',
        'footer_icon':'https://12oz.glass/static/warning.png',
        'attachment_type':'default',
        'color':'#D13C3C',
    }]
    # Post reopened issue message to thread
    log.info('Returning updated reopen message')
    return json.dumps({'attachments':attach})


# Process slash commands
# Currently unused
# def command(cmd, data):
#     if cmd == '/hello':
#         return json.dumps({"text": "DragonBot says hello!", "response_type": "ephemeral"})
#     else:
#         return json.dumps({"text": "What is zig?", "response_type": "ephemeral"})
