#!/usr/local/bin/python3.7

from bottle import request,response
import bottle
import slack
import logging
import logging.handlers
import time

# Logging configuration
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address = '/dev/log')
formatter = logging.Formatter('dragonbot.%(module)s.%(funcName)s: %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

# Initialize Bottle application
app = application = bottle.Bottle()

# Extract Slack request authentication headers to return timestamp and signature for validation
def extract_auth_headers(headers):
    log.info('Attempting auth header translation')
    log.debug('Extract request.headers[\'X-Slack-Request-Timestamp\']')
    try:
        ts = str(headers['X-Slack-Request-Timestamp'])
    except KeyError:
        log.warn('Missing X-Slack-Request-Timestamp from incoming request')
        return None, None
    else:
        log.debug(f'ts = {ts}')
    log.debug('Extract requests.headers[\'X-Slack-Signature\']')
    try:
        sig = str(headers['X-Slack-Signature'])
    except KeyError:
        log.warn('Missing X-Slack-Signature from incoming request')
        return None, None
    else:
        log.debug(f'sig = {sig}')
    log.info('Extracted auth header values successfully')
    return ts, sig


# Return placeholder for GET requests to /
@app.get('/')
def show_index():
    log.error('Incoming GET on route / - check reverse proxy')
    return 'DragonBot says hello!'


# Return placeholder for GET requests to /DragonBot
@app.get('/dragonbot')
@app.get('/dragonbot/')
def show_hello():
    log.info('Incoming GET on route /dragonbot')
    return 'DragonBot says hello!'
    

# Endpoint for Slack OAuth flow, exchanges received code for oauth2 token
@app.get('/dragonbot/oauth')
@app.get('/dragonbot/oauth/')
def oauth():
    log.info('Incoming GET on route /dragonbot/oauth')
    try:
        # Snag oauth code from query string to exchange for token
        code = request.query.code
        log.info(f'Received oauth code: {code}')
    except:
        log.warn('Unable to parse oauth code')
        response.status = 400
        return '400 Bad Request'
    return slack.oauth(code)


# Endpoint for Slack event subscriptions
@app.post('/dragonbot/event')
@app.post('/dragonbot/event/')
def event():
    log.info('Incoming POST on route /dragonbot/event')
    # Extract headers and validate timestamp, read request body
    try:
        ts,sig = extract_auth_headers(request.headers)
        if not ts or not sig:
            log.warn('Invalid or missing auth headers - rejecting request')
            response.status = 403
            return '403 Forbidden'
        if abs(time.time() - float(ts)) > 150:
            log.warn(f'Request timestamp differs from current time by {abs(time.time() - float(ts))} seconds - rejecting request')
            response.status = 403
            return '403 Forbidden'
        body = request.body.read()
    except:
        log.error('Auth header extraction failed')
        response.status = 403
        return '403 Forbidden'
    log.debug('Exec slack.auth_request()')
    # Verify signature
    if not slack.verify_request(ts, sig, body):
        log.warn('Authentication failed - returning 403 Forbidden')
        response.status = 403
        return '403 Forbidden'
    # 400 response generated automatically for invalid JSON
    data = request.json
    response.headers['Content-Type'] = 'application/json'
    # Will generally be returning empty string, but new message data can be returned instead
    return slack.event(data)


# Endpoint for Slack interactive message callbacks
@app.post('/dragonbot/callback')
@app.post('/dragonbot/callback/')
def callback():
    log.info('Incoming POST on route /dragonbot/callback')
    # Extract headers and validate timestamp, read request body
    try:
        ts,sig = extract_auth_headers(request.headers)
        if not ts or not sig:
            log.warn('Invalid or missing auth headers - rejecting request')
            response.status = 403
            return '403 Forbidden'
        if abs(time.time() - float(ts)) > 150:
            log.warn(f'Request time differs by {abs(time.time() - float(ts))} seconds - rejecting request')
            response.status = 403
            return '403 Forbidden'
        body = request.body.read()
    except:
        log.error('Auth header extraction failed')
        response.status = 403
        return '403 Forbidden'
    log.debug('Exec slack.auth_request()')
    # Verify signature
    if not slack.verify_request(ts, sig, body):
        log.warn('Authetication failed - returning 403 Forbidden')
        response.status = 403
        return '403 Forbidden'
    # 400 response generated automatically for missing/inalid body
    resp = request.forms.get('payload')
    response.headers['Content-Type'] = 'application/json'
    return slack.callback(resp)


# Endpoint for Slack slash commands
# Note: This is for future use - there are currently no slash commands implemented and this handler is very incomplete
# @app.post('/dragonbot/command')
# @app.post('/dragonbot/command/')
# def command():
#     log.info('Incoming POST on route /dragonbot/command')
#     log.debug('Attempting auth header translation')
#     try:
#         log.debug('Extract request.headers[\'X-Slack-Request-Timestamp\']')
#         ts = str(request.headers['X-Slack-Request-Timestamp'])
#         log.debug('ts = %s' % ts)
#         if abs(time.time() - float(ts)) > 150:
#             log.warn('Request time differs by %s seconds - rejecting request' % str(abs(time.time() - float(ts))))
#             response.status = 403
#             return '403 Forbidden'
#         log.debug('Extract requests.headers[\'X-Slack-Signature\']')
#         sig = str(request.headers['X-Slack-Signature'])
#         log.debug('sig = %s' % sig)
#         log.debug('Extract request.body')
#         body = request.body.read()
#         log.debug('Header extraction complete')
#     except:
#         log.error('Auth header extraction failed')
#         response.status = 403
#         return '403 Forbidden'
#     log.debug('Exec slack.auth_request()')
#     if not slack.auth_request(ts,sig,body):
#         log.warn('Authorization failed - returning 403 Forbidden')
#         response.status = 403
#         return '403 Forbidden'
#     try:
#         token = request.forms.get('token')
#         cmd = request.forms.get('command')
#         data = request.forms.get('text')
#     except:
#         raise ValueError
#     result = slack.command(cmd, data)
#     response.headers['Content-Type'] = 'application/json'
#     return result
