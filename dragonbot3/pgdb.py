#!/usr/local/bin/python3.7

import psycopg2
import logging
import logging.handlers

# Logging configuration
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address = '/dev/log')
formatter = logging.Formatter('dragonbot.%(module)s.%(funcName)s: %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

# Start database connection, return open connection and cursor objects
def dbconnect():
    try:
        conn = psycopg2.connect('dbname=dragoncon user=nginx')
        cur = conn.cursor()
    except BaseException as e:
        log.error(f'Failed to db connect: {repr(e)}') 
    else:
        return conn,cur


# Closes open databse connections after rolling back any open transactions
def dbdisconnect(conn, cur):
    try:
        conn.rollback()
        cur.close()
        conn.close()
    except:
        pass


# Retrieve key value from database, optionally accepts team_id to identify which Slack team the key belongs to
def get_key_value(key_type, team=None):
    log.info(f'Selecting key of type "{key_type}" from database')
    try:
        conn, cur = dbconnect()
        try:
            if not team:
                cur.execute('SELECT key_value FROM keys WHERE key_type = %s AND team_id IS NULL)', (key_type,))
            else:
                cur.execute('SELECT key_value FROM keys WHERE key_type = %s AND team_id = %s', (key_type,team,))
            res = cur.fetchone()
            if res is None or len(res) == 0:
                log.warn(f'Key of type "{key_type}" not found')
                return None
            else:
                log.info(f'Key of type "{key_type}" found')
                return res[0]
        except BaseException as e:
            log.warn(f'Error selecting key value from database: {repr(e)}')
    except BaseException as e:
        log.error(f'Unable to connect to database: {repr(e)}')
        return None
    finally:
        dbdisconnect(conn, cur)


# Insert key value to keys table, fails on constraint violation, optionally accepts team_id to identify which Slack team the key belongs to
def insert_key_value(key_type, key_value, team=None):
    log.info(f'Inserting key of type "{key_type}" into database')
    try:
        conn, cur = dbconnect()
        try:
            if not team:
                cur.execute('INSERT INTO keys (key_type, key_value) VALUES(%s, %s, %s)', (key_type, key_value,))
            else:
                cur.execute('INSERT INTO keys (key_type, key_value, team_id) VALUES(%s, %s, %s)', (key_type, team,))
            conn.commit()
        except BaseException as e:
            log.warn(f'Error inserting key value into database: {repr(e)}')
            return False
    except BaseException as e:
        log.error(f'Unable to connect to database: {repr(e)}')
        return False
    else:
        log.info(f'Key of type {key_type} inserted successfully')
        return True
    finally:
        dbdisconnect(conn, cur)


# Insert key value to database, updates existing value on conflict, optionally accepts team_id to identify which Slack team the key belongs to
def upsert_key_value(key_type, key_value, team=None):
    log.info(f'Inserting key of type "{key_type}" into database')
    try:
        conn, cur = dbconnect()
        try:
            if not team:
                cur.execute('INSERT INTO keys (key_type, key_value) VALUES(%s, %s) \
                            ON CONFLICT (key_type) DO UPDATE SET key_value = EXCLUDED.key_value', (key_type, key_value,))
            else:
                cur.execute('INSERT INTO keys (key_type, key_value, team_id) VALUES(%s, %s, %s) \
                            ON CONFLICT (key_type, team_id) DO UPDATE SET key_value = EXCLUDED.key_value', (key_type, key_value, team,))
            conn.commit()
        except BaseException as e:
            log.warn(f'Error inserting key value into database: {repr(e)}')
            return False
    except BaseException as e:
        log.error(f'Unable to connect to database: {repr(e)}')
        return False
    else:
        log.info(f'Key of type {key_type} upserted successfully')
        return True
    finally:
        dbdisconnect(conn, cur)


# Retrieve a logged issue from the database based on original issue message's timestamp
def retrieve_issue(ts):
    log.info(f'Retrieving issue from database, ts: {ts}')
    try:
        conn, cur = dbconnect()
        try:
            cur.execute('SELECT title, body FROM issue_log WHERE ts = %s', (ts,))
            res = cur.fetchone()
            if res is None or len(res) == 0:
                log.warn(f'Issue with timestamp {ts} not found')
                return None
            else:
                log.info(f'Issue with timestamp {ts} located: {res[0]}')
                return res[0], res[1] # title, body
        except BaseException as e:
            log.warn(f'Error selecting key value from database: {repr(e)}')
    except BaseException as e:
        log.error(f'Unable to connect to database: {repr(e)}')
        return None
    finally:
        dbdisconnect(conn, cur)


# Log issue to Database
# Used to avoid requirement for app to read all messages in a channel
# That method is dependent on individual user permissions, as bots don't have access in and of themselves
def log_issue(ts, title, txt):
    log.info(f'Logging issue to database, ts: {ts}')
    try:
        conn, cur = dbconnect()
        try:
            cur.execute('INSERT INTO issue_log (ts, title, body) values(%s, %s, %s)', (ts, title, txt,))
            conn.commit()
        except BaseException as e:
            log.warn(f'Error inserting issue into database: {repr(e)}')
            return False
    except BaseException as e:
        log.error(f'Unable to connect to database: {repr(e)}')
        return False
    else:
        log.info(f'Issue with timestamp {ts} inserted successsfully: {title}')
        return True
    finally:
        dbdisconnect(conn, cur)
